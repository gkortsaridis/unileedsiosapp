//
//  TabVarViewController.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 20/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit

class TabVarViewController: UITabBarController {

    let modulesRepository = ModulesRepository()
    let facultiesRepository = FacultiesRepository()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        facultiesRepository.retrieveFaculties{ (faculties: [UniversityFaculty]) in
                           
        }
                
               
               
        let startupScreen = UserDefaults.standard.string(forKey: "startupScreen") ?? ""
        if startupScreen == "mySchedule" {
            self.selectedIndex = 1
        }else{
            self.selectedIndex = 0
        }
        
        
    }
    
}
