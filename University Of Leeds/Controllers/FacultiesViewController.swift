//
//  FacultiesViewController.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 10/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit

class FacultiesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    let facultiesRepository = FacultiesRepository()
    var universityFaculties:[UniversityFaculty]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.universityFaculties = facultiesRepository.getAllFaculties()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    
        self.tableView.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return universityFaculties?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FacultyCell", for: indexPath)
        
        cell.textLabel?.text = universityFaculties![indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "FacultyInfoViewController") as! FacultyInfoViewController
        newViewController.faculty = universityFaculties![indexPath.row]
        self.present(newViewController, animated: true)
    }
    
}
