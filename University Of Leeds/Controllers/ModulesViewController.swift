//
//  ModulesViewController.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 07/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit

class ModulesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickerView: UIView!
    

    var universitySchools:[UniversitySchool] = []
    var schoolRepository = SchoolRepository()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        universitySchools = schoolRepository.getAllSchools()
        
        tableView.delegate = self
        tableView.dataSource = self
        

        self.pickerView.dropShadow()

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return universitySchools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        
        cell.textLabel?.text = universitySchools[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ModuleListViewController") as! ModuleListViewController
        newViewController.selectedSchool = universitySchools[indexPath.row]
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    
    
}
