//
//  MyCalendarViewController.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 03/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit
import EzPopup

class MyCalendarViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    let timetableRepository = TimetableRepository()
    let modulesRepository = ModulesRepository()
    var timetables: [UniversityTimetableEntry]?
    var loading: Bool = true
    
    override func viewWillAppear(_ animated: Bool) {
        determineTimetables()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        loading = true
        self.tableView.reloadData()
        self.modulesRepository.retrieveModules{ (modules: [UniversityModule]) in
            self.loading = false
            self.determineTimetables()
        }
        
        
    }
    
    func determineTimetables(){
        let savedModules = modulesRepository.getSavedModules()
        print("Saved Modules "+savedModules.count.description)
        timetableRepository.createTimetableList(forModules: savedModules) { (data: [UniversityTimetableEntry]?) in
            self.timetables = data
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if timetables?.count ?? 0 > 0{
            let timetable = self.timetables![indexPath.row]
            
            if timetable.isError(){
                //Displaying "We encountered an error"
                return 120
            }else if timetable.module?.moduleID == "-1"{
                //Displaying Day of Week
                return 50
            }else{
                //Displaying Timetable Entry
                return 80
            }
        }else{
            //Displaying "We encountered an error"
            return 120
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.timetables?.count ?? 01 > 0{
            return self.timetables?.count ?? 0
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        if self.loading {
            let cell = tableView.dequeueReusableCell(withIdentifier: "timetable_loading_cell")!
            return cell
        }else if timetables?.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyListCell")! as! EmptyListTableViewCell
            
            return cell
        }else{
            let timetable = self.timetables![indexPath.row]
            
            if timetable.isError(){
                let cell = tableView.dequeueReusableCell(withIdentifier: "timetable_error_cell")!
                return cell
            }else{
                if timetable.module?.moduleID == "-1"{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DayHeaderCell")! as! TimetableHeaderTableViewCell
                    cell.dayLabel.text = timetable.weekday
                    
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TimetableCell")! as! TimetableTableViewCell
                    
                    cell.moduleName.text = timetable.module?.name
                    cell.moduleTeacher.text = timetable.staff
                    cell.timetableTime.text = timetable.startTime.count == 5 ? timetable.startTime : " "+timetable.startTime
                    cell.timetableEndTime.text = timetable.finishTime.count == 5 ? timetable.finishTime : " "+timetable.finishTime
                    cell.moduleRoom.text = timetable.location
                    
                    let hexColor = String(format:"%06X", (0xFFFFFF & (timetable.module?.moduleID.hashCode())!));
                    let otherColor = UIColor(hexRGB: "#"+hexColor) ?? UIColor.blue
                    cell.stripe.backgroundColor = otherColor
                    return cell
                }
            }
            
            
        }
      
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if timetables?.count ?? 0 > 0{
            if timetables![indexPath.row].module?.moduleID != "-1"{
                let contentVC = storyBoard.instantiateViewController(withIdentifier: "TimetablePopupViewController") as! TimetablePopupViewController
                contentVC.navController = self.navigationController!
                contentVC.timetable = timetables![indexPath.row]
                let deviceWidth = UIScreen.main.bounds.width
                let height:CGFloat = timetables![indexPath.row].universityLocation.isValidLocation() ? 310 : 250
                let popupVC = PopupViewController(contentController: contentVC, popupWidth: deviceWidth-40, popupHeight: height)
                present(popupVC, animated: true)
            }
        }else{
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ModulesViewController") as! ModulesViewController
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
    }
}
