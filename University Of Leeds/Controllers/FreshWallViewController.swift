//
//  FreshWallViewController.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 03/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit
import XLPagerTabStrip


class FreshWallViewController: SegmentedPagerTabStripViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override public func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NewsViewController") as! NewsViewController
        let child_2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EventsViewController") as! EventsViewController
        return [child_1, child_2]
        
    }

}
