//
//  NewsViewController.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 03/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SDWebImage

class NewsViewController: UIViewController, IndicatorInfoProvider, UITableViewDataSource, UITableViewDelegate {
    

    @IBOutlet weak var tableView: UITableView!
    
    let newsRepository = NewsRepository()
    
    var allNews: [UniversityNew] = []
    var allEvents: [UniversityNew] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newsRepository.retrieveNews { (news: [UniversityNew], events: [UniversityNew]) in
            self.allNews = news
            self.allEvents = events
            self.tableView.reloadData()
        }
                
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 120
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "News")
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let new = allNews[indexPath.row]

        if new.isError(){
            let cell = tableView.dequeueReusableCell(withIdentifier: "news_error_cell")!
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "news_cell")! as! NewsTableViewCell
            
            cell.title.text = new.title
            cell.information.text = new.category
            cell.datetime.text = new.datetime
            
            cell.cardContainer.layer.cornerRadius = 5
            cell.cardContainer.clipsToBounds = true
            
            if new.img_url != "" {
                let imageURL = URL(string: "http://"+new.img_url)
                if imageURL != nil {
                    cell.backgroundImg.sd_setImage(with: imageURL)
                }else{
                    cell.backgroundImg.backgroundColor = UIColor.blue
                }
            }else{
                cell.backgroundImg.isHidden = true
                cell.cardContainer.backgroundColor = UIColor.init(hexRGB: "871414")
            }
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let new = allNews[indexPath.row]
        guard let url = URL(string: "https://"+new.link_url) else {
            return
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
}
