//
//  ModuleListViewController.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 09/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit

class ModuleListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var schoolTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var moduleSearch: UISearchBar!
    
    var selectedSchool:UniversitySchool?

    var modulesRepository = ModulesRepository()
    var modules:[UniversityModule] = []
    var unfilteredModules:[UniversityModule] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        modules = modulesRepository.getModulesForSchool(school: selectedSchool!)
        unfilteredModules = modules
        
        tableView.dataSource = self
        tableView.delegate = self
        moduleSearch.delegate = self

        tableView.rowHeight = 90
        
        schoolTitle.text = selectedSchool?.name ?? "Unknown"
      
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        modules = modulesRepository.filterModules(modules: unfilteredModules, filter: searchText)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modules.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let module = modules[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ModuleListTableViewCell")! as! ModuleListTableViewCell

        cell.moduleName.text = module.name
        cell.isSaved.setOn(modulesRepository.isModuleSaved(module: module), animated: false)
        cell.isSaved.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
        cell.isSaved.tag = indexPath.row
        
        cell.graduateLevel.text = module.moduleID+" ("+(module.isUndergraduate ? "UG" : "PG")+")"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @objc func switchValueDidChange(_ sender: UISwitch) {
        modulesRepository.setModuleSaved(module: modules[sender.tag], shouldSave: sender.isOn)
    }
    
}
