//
//  UniversityInfoViewController.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 09/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit

class UniversityInfoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 270
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UniversityInfoTableViewCell")! as! UniversityInfoTableViewCell
        cell.selectionStyle = .none

        cell.cardView.dropShadow()
        
        if indexPath.row == 0 {
            cell.cardSubtitle.text = "Discover University"
            cell.cardTitle.text = "Faculties"
        }else if indexPath.row == 1{
            cell.cardSubtitle.text = "List University"
            cell.cardTitle.text = "Modules"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if indexPath.row == 0{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "FacultiesViewController") as! FacultiesViewController
            self.navigationController?.pushViewController(newViewController, animated: true)
        }else if indexPath.row == 1{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ModulesViewController") as! ModulesViewController
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
    }
    
}
