//
//  EventsViewController.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 03/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class EventsViewController: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource {
 
    @IBOutlet weak var tableView: UITableView!
    
    var allEvents: [UniversityNew] = []

    override func viewWillAppear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.allEvents = appDelegate.universityEvents ?? []

        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.rowHeight = 120
            
        self.tableView.reloadData()
    }

        
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Events")
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allEvents.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let event = allEvents[indexPath.row]
        
        if event.isError(){
            let cell = tableView.dequeueReusableCell(withIdentifier: "news_error_cell")!
                
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "news_cell")! as! NewsTableViewCell
                
            cell.title.text = event.title
            cell.information.text = event.category
            cell.datetime.text = event.datetime
                
            cell.cardContainer.layer.cornerRadius = 5
            cell.cardContainer.clipsToBounds = true
                
            if event.img_url != "" {
                let imageURL = URL(string: "http://"+event.img_url)
                if imageURL != nil {
                    cell.backgroundImg.sd_setImage(with: imageURL)
                }else{
                    cell.backgroundImg.backgroundColor = UIColor.blue
                }
            }else{
                cell.backgroundImg.isHidden = true
                cell.cardContainer.backgroundColor = UIColor.init(hexRGB: "871414")
            }
                
            return cell
        }
            
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
            
        let event = allEvents[indexPath.row]
        guard let url = URL(string: "https://"+event.link_url) else {
            return
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
}
