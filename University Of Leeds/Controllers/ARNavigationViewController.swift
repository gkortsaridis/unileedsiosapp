//
//  ARNavigationViewController.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 23/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit
import ARCL
import CoreLocation
import MapKit

class ARNavigationViewController: UIViewController, CLLocationManagerDelegate, LNTouchDelegate {

    let locationManager = CLLocationManager()
    var sceneLocationView = SceneLocationView()
    
    var timetableEntry:UniversityTimetableEntry?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        sceneLocationView.run()
        sceneLocationView.locationNodeTouchDelegate = self
        view.addSubview(sceneLocationView)
    }
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sceneLocationView.frame = view.bounds
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }

        let poi_location = CLLocation(coordinate: (timetableEntry?.universityLocation.location)!, altitude: CLLocationDistance(((timetableEntry?.universityLocation.altitude)!)))
        let distanceMeters = CLLocation(coordinate: locValue, altitude: CLLocationDistance((timetableEntry?.universityLocation.altitude)!)).distance(from: poi_location)
        let view = POI_ARView(title: timetableEntry!.location, distance: Int(distanceMeters).description)
        
        let annotationNode = LocationAnnotationNode(location: poi_location, view: view)
        //annotationNode.scaleRelativeToDistance = true
        sceneLocationView.removeAllNodes()
        sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: annotationNode)
    }
    
    func locationNodeTouched(node: AnnotationNode) {
        print(node)
    }


}


