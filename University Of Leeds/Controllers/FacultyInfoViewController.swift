//
//  FacultyInfoViewController.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 11/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit

class FacultyInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var facultyName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var facultyWebsite: UIButton!
    
    var faculty:UniversityFaculty?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        facultyName.text = faculty?.name
        facultyWebsite.setTitle(faculty?.website, for: .normal)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return faculty!.info.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FacultyInfoTableViewCell", for: indexPath)
        cell.textLabel?.text = faculty?.info[indexPath.row].key
        cell.detailTextLabel?.text = faculty?.info[indexPath.row].value
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @IBAction func openWebsite(_ sender: Any) {
    }
    
}
