//
//  EmptyListTableViewCell.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 10/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit

class EmptyListTableViewCell: UITableViewCell {

    @IBOutlet weak var goToModulesButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
