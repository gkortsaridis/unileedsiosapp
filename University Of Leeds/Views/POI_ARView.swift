//
//  POI_ARView.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 23/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit

class POI_ARView: UIView {
    
    override init (frame : CGRect) {
        super.init(frame : frame)
    }
    
    convenience init (title: String, distance: String) {
        self.init(frame:CGRect(x: 0,y: 0,width: 200,height: 75))

        let topContainer = UIView()
        topContainer.frame = CGRect(x: 0,y: 0,width: 200,height: 50)
        topContainer.backgroundColor = UIColor(red: 242, green: 247, blue: 255, alpha: 0.2)
        topContainer.layer.cornerRadius = 10
        
        let logo = UIImage(named: "logo_square")
        let imageView = UIImageView(image: logo)
        imageView.frame = CGRect(x: 5, y: 5, width: 40, height: 40)
        
        let text = UILabel()
        text.frame = CGRect(x: 50, y: 0, width: 160, height: 25)
        text.text = title
        text.textColor = UIColor.black
        text.backgroundColor = UIColor.clear
        
        let text2 = UILabel()
        text2.frame = CGRect(x: 50, y: 25, width: 160, height: 25)
        text2.text = distance+"m"
        
        let triangle = Triangle()
        triangle.color = UIColor(red: 242, green: 247, blue: 255, alpha: 0.2)
        triangle.frame = CGRect(x: 62.5, y: 50, width: 75, height: 25)
        
        self.addSubview(topContainer)
        self.addSubview(imageView)
        self.addSubview(text)
        self.addSubview(text2)
        self.addSubview(triangle)
    }
    
    convenience required init(coder aDecoder: NSCoder) {
        self.init(frame:CGRect(x: 0,y: 0,width: 200,height: 50))
    }
    
    func displayExtras(){
        print("My Message!")
    }

}

