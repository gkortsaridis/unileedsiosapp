//
//  TimetableTableViewCell.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 03/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit

class TimetableTableViewCell: UITableViewCell {
    @IBOutlet weak var moduleName: UILabel!
    @IBOutlet weak var timetableTime: UILabel!
    @IBOutlet weak var moduleRoom: UILabel!
    @IBOutlet weak var moduleTeacher: UILabel!
    @IBOutlet weak var timetableEndTime: UILabel!
    @IBOutlet weak var stripe: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
