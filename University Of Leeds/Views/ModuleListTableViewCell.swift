//
//  ModuleListTableViewCell.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 09/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit

class ModuleListTableViewCell: UITableViewCell {

    @IBOutlet weak var moduleName: UILabel!
    @IBOutlet weak var isSaved: UISwitch!
    @IBOutlet weak var graduateLevel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
