//
//  UniversityTimetableEntry.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 03/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import Foundation

class UniversityTimetableEntry{
    
    var module: UniversityModule?
    var activity: String = ""
    var parentActivity: String = ""
    var location: String = ""
    var weekday: String = ""
    var startTime: String = ""
    var finishTime: String = ""
    var teachingWeeksUniWeeks: String = ""
    var teachingWeeksSplusWeeks: String = ""
    var staff: String = ""
    var recordingStatus: String = ""
    var universityLocation: UniversityLocation

    init(module: UniversityModule, activity: String, parentActivity: String, location: String, weekday: String, startTime: String, finishTime: String, teachingWeeksUniWeeks: String, teachingWeeksSplusWeeks: String, staff: String, recordingStatus: String){
        
        self.module = module
        self.activity = activity
        self.parentActivity = parentActivity
        self.location = location
        self.weekday = weekday
        self.startTime = startTime
        self.finishTime = finishTime
        self.teachingWeeksUniWeeks = teachingWeeksUniWeeks
        self.teachingWeeksSplusWeeks = teachingWeeksSplusWeeks
        self.staff = staff
        self.recordingStatus = recordingStatus
        
        self.universityLocation = UniversityLocation(name:self.location)
    }
    
    init(timetableObject: [String:Any]){
        self.module = UniversityModule(moduleObject: timetableObject["_module"]! as! [String:Any])
        self.activity = timetableObject["_activity"] as? String ?? ""
        self.parentActivity = timetableObject["_parentActivity"] as? String ?? ""
        self.location = timetableObject["_location"] as? String ?? ""
        self.weekday = timetableObject["_weekday"] as? String ?? ""
        self.startTime = timetableObject["_startTime"] as? String ?? ""
        self.finishTime = timetableObject["_finishTime"] as? String ?? ""
        self.teachingWeeksUniWeeks = timetableObject["_teachingWeeksUniWeeks"] as? String ?? ""
        self.teachingWeeksSplusWeeks = timetableObject["_teachingWeeksSplusWeeks"] as? String ?? ""
        self.staff = timetableObject["_staff"] as? String ?? ""
        self.recordingStatus = timetableObject["_recordingStatus"] as? String ?? ""
        self.universityLocation = UniversityLocation(name:self.location)

    }
    
    func isError()->Bool{
        return activity == "-1"
            && parentActivity == "-1"
            && location == "-1"
            && weekday == "-1"
            && startTime == "-1"
            && finishTime == "-1"
            && teachingWeeksUniWeeks == "-1"
            && teachingWeeksSplusWeeks == "-1"
            && staff == "-1"
            && recordingStatus == "-1"
    }
    
}
