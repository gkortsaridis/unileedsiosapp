//
//  UniversityFaculty.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 10/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import Foundation

class UniversityFaculty{
    
    var name: String
    var website: String
    var info:[KeyValueUrl]
    
    init(name: String, website:String, info:[KeyValueUrl]){
        self.name = name
        self.website = website
        self.info = info
    }
   
    init(universityFacultyObject: [String:Any]){
        self.name = universityFacultyObject["_name"] as? String ?? ""
        self.website = universityFacultyObject["_website"] as? String ?? ""
        
        let infoArr = universityFacultyObject["_info"] as! [[String:Any]]
        var infos:[KeyValueUrl] = []
        for i in 0...infoArr.count-1{
            infos.append(KeyValueUrl(keyValUrlObject: infoArr[i]))
        }
        self.info = infos
        
    }
    
}
