//
//  UniversitySchool.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 09/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import Foundation

class UniversitySchool{
    
    var schoolID: String = ""
    var name: String = ""
    
    init(schoolID: String, name: String){
        self.schoolID = schoolID
        self.name = name
    }
    
}
