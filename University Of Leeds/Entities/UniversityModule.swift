//
//  UniversityModule.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 03/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import Foundation

class UniversityModule{
    
    var moduleID: String = ""
    var name: String = ""
    var isDiscoveryModule: Bool = false
    var isAvailableForIncomingStudyAbroad: Bool = false
    var schoolID: String = ""
    var isUndergraduate: Bool = false

    init(moduleID: String, name: String, isDiscoveryModule: Bool, isAvailableForIncomingStudyAbroad: Bool, schoolID: String, isUndergraduate: Bool){
        self.moduleID = moduleID
        self.name = name
        self.isDiscoveryModule = isDiscoveryModule
        self.isAvailableForIncomingStudyAbroad = isAvailableForIncomingStudyAbroad
        self.schoolID = schoolID
        self.isUndergraduate = isUndergraduate
    }
    
    init(moduleObject: [String:Any]){
        self.schoolID = moduleObject["_schoolID"] as? String ?? ""
        self.moduleID = moduleObject["_moduleID"] as? String ?? ""
        self.name = moduleObject["_name"] as? String ?? ""
        self.isDiscoveryModule = moduleObject["_isDiscoveryModule"] as? Int == 1
        self.isAvailableForIncomingStudyAbroad = moduleObject["_isAvailableForIncomingStudyAbroad"] as? Int == 1
        self.isUndergraduate = moduleObject["_isUndergraduate"] as? Int == 1
    }
    
    func isError()->Bool{
        return schoolID == "-1"
            && moduleID == "-1"
            && name == "-1"
            && isDiscoveryModule == false
            && isAvailableForIncomingStudyAbroad == false
            && isUndergraduate == false
    }
    
}
