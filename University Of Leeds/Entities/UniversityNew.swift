//
//  UniversityNew.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 03/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import Foundation

class UniversityNew{
    
    var title: String = ""
    var category: String = ""
    var datetime: String = ""
    var img_url: String = ""
    var link_url: String = ""
    
    init(title: String, category: String, datetime: String, img_url: String, link_url: String){
        self.title = title
        self.category = category
        self.datetime = datetime
        self.img_url = img_url
        self.link_url = link_url
    }
    
    init(newsObject: [String:Any]){
        self.title = newsObject["_title"] as? String ?? ""
        self.category = newsObject["_category"] as? String ?? ""
        self.datetime = newsObject["_datetime"] as? String ?? ""
        self.img_url = newsObject["_img_url"] as? String ?? ""
        self.link_url = newsObject["_link_url"] as? String ?? ""
    }
    
    func isError()->Bool{
        return title == "-1"
            && category == "-1"
            && datetime == "-1"
            && img_url == "-1"
            && link_url == "-1"
    }
    
}
