//
//  UniversityLocation.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 23/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import Foundation
import CoreLocation


class UniversityLocation{
    
    var name:String
    var location:CLLocationCoordinate2D
    var altitude:Int
    
    let noLocation =  CLLocationCoordinate2D(latitude: 0, longitude: 0)
    
    init(name:String){
        self.name = name
        
        if name.contains("Roger Stevens"){
            self.location = CLLocationCoordinate2D(latitude: 53.805084, longitude: -1.554690)
            self.altitude = 70
        }else{
            self.location = noLocation
            self.altitude = 0
        }
    }
    
    func isValidLocation()->Bool{
        return (self.location.latitude != noLocation.latitude && self.location.longitude != noLocation.longitude)
    }
    
}
