//
//  HTTPEntity.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 10/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import Foundation
import Alamofire

class HTTPEntity{
    
    let urlDomain = "http://52.56.59.182:8080/"
    //let urlDomain = "http://10.41.110.19:8080/"
    
    func doPost(url: String, withParameters: Parameters, completion: @escaping (_ data: [String:Any]?) -> ()){
        Alamofire.request(self.urlDomain+url, method: .post, parameters: withParameters, encoding: JSONEncoding.default).responseString { response in
            completion(response.result.value?.convertToJSONObject())
        }
    }
    
    func doPost(url: String, completion: @escaping (_ data: [String:Any]?) -> ()){
        Alamofire.request(self.urlDomain+url, method: .post).responseString { response in
            completion(response.result.value?.convertToJSONObject())
        }
    }
    
}
