//
//  KeyValueUrl.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 10/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import Foundation

class KeyValueUrl{
    
    var key:String
    var value:String
    var url:String

    init(key:String, value:String, url:String){
        self.key = key
        self.value = value
        self.url = url
    }
    
    init(keyValUrlObject: [String:Any]){
        self.key = keyValUrlObject["_key"] as? String ?? ""
        self.value = keyValUrlObject["_value"] as? String ?? ""
        self.url = keyValUrlObject["_url"] as? String ?? ""
    }
}
