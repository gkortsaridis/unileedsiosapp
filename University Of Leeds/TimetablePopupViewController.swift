//
//  TimetablePopupViewController.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 20/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit

class TimetablePopupViewController: UIViewController {

    @IBOutlet weak var moduleName: UILabel!
    @IBOutlet weak var timetableDate: UILabel!
    @IBOutlet weak var timetableTeacher: UILabel!
    @IBOutlet weak var timetableLocation: UILabel!
    @IBOutlet weak var navigateBtn: UIButton!
    
    
    var navController: UINavigationController?
    var timetable:UniversityTimetableEntry?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(timetable?.module?.moduleID)
        
        moduleName.text = (timetable?.module!.moduleID)!+" "+(timetable?.module!.name)!
        var date = timetable!.weekday+" "
        date = date+timetable!.startTime
        date = date+" - "
        date = date+timetable!.finishTime
        timetableDate.text = date
        timetableTeacher.text = timetable!.staff
        timetableLocation.text = timetable?.location
        
        if !(timetable?.universityLocation.isValidLocation())!{
            navigateBtn.isHidden = true
        }
    }

    @IBAction func locationPressed(_ sender: Any) {
        navController!.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
        let arNavigation = ARNavigationViewController()
        arNavigation.timetableEntry = timetable
        self.navController!.pushViewController(arNavigation, animated: true)
    }
}
