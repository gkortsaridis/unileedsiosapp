//
//  SchoolRepository.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 09/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import Foundation

class SchoolRepository {
 
    func getAllSchools()->[UniversitySchool]{
        return [
            UniversitySchool(schoolID: "BIOC", name: "Biochemistry and Molecular Biology"),
            UniversitySchool(schoolID: "BIOL", name: "Biological Sciences"),
            UniversitySchool(schoolID: "BLGY", name: "Biology"),
            UniversitySchool(schoolID: "MEDP", name: "Biomedical Imaging & Healthcare Science"),
            UniversitySchool(schoolID: "BISC", name: "Biomedical Sciences"),
            UniversitySchool(schoolID: "LUBS", name: "Business School"),
            UniversitySchool(schoolID: "CSER", name: "Careers Centre"),
            UniversitySchool(schoolID: "PREN", name: "Chemical & Process Engineering"),
            UniversitySchool(schoolID: "CHEM", name: "Chemistry"),
            UniversitySchool(schoolID: "CIVE", name: "Civil Engineering"),
            UniversitySchool(schoolID: "COLO", name: "Colour Science"),
            UniversitySchool(schoolID: "COMP", name: "Computing"),
            UniversitySchool(schoolID: "DENT", name: "Dentistry"),
            UniversitySchool(schoolID: "DESN", name: "Design"),
            UniversitySchool(schoolID: "ODLC", name: "Digital Education Service"),
            UniversitySchool(schoolID: "SOEE", name: "Earth & Environment"),
            UniversitySchool(schoolID: "EDUC", name: "Education"),
            UniversitySchool(schoolID: "ELEC", name: "Electornic & Electrical Engineering"),
            UniversitySchool(schoolID: "ENGL", name: "English"),
            UniversitySchool(schoolID: "EPIB", name: "Epidemiology & Biostatistics"),
            UniversitySchool(schoolID: "ARTF", name: "Fine Art, History of Art & Cultural Studies"),
            UniversitySchool(schoolID: "FOOD", name: "Food Science & Nutrition"),
            UniversitySchool(schoolID: "FOUN", name: "Foundation Programmes"),
            UniversitySchool(schoolID: "GEOG", name: "Geography"),
            UniversitySchool(schoolID: "HECS", name: "Healthcare"),
            UniversitySchool(schoolID: "HIST", name: "History"),
            UniversitySchool(schoolID: "IDEA", name: "Inter-Disciplinary Ethics Applied (CETL)"),
            UniversitySchool(schoolID: "JEST", name: "Jewish Studies"),
            UniversitySchool(schoolID: "ELU", name: "Language Centre"),
            UniversitySchool(schoolID: "AMES", name: "(Arabic, Islamic & Middle Easter Studies) Languages, Cult & Soc"),
            UniversitySchool(schoolID: "CLAS", name: "(Classics) Languages, Cult & Soc"),
            UniversitySchool(schoolID: "EAST", name: "(East Asian Studies) Language, Cult & Soc"),
            UniversitySchool(schoolID: "FREN", name: "(French) Languages, Cult & Soc"),
            UniversitySchool(schoolID: "GERM", name: "(German) Languages, Cult & Soc"),
            UniversitySchool(schoolID: "ITAL", name: "(Italian) Languages, Cult & Soc"),
            UniversitySchool(schoolID: "SMLC", name: "(Joint Honours) Languages, Cult & Soc"),
            UniversitySchool(schoolID: "FLTU", name: "(Languages for All) Languages, Cult & Soc"),
            UniversitySchool(schoolID: "LING", name: "(Linguistics and Pronetics) Languages, Cult & Soc"),
            UniversitySchool(schoolID: "MODL", name: "(Modern Languages and Cultures) Languages, Cult & Soc"),
            UniversitySchool(schoolID: "RUSL", name: "(Russian and Slavonic Studies) Languages, Cult & Soc"),
            UniversitySchool(schoolID: "SPPO", name: "(Spanish, Portugese and Latin American Law) Languages, Cult & Soc"),
            UniversitySchool(schoolID: "LAW", name: "Law"),
            UniversitySchool(schoolID: "LIHS", name: "Leeds Institute of Health Sciences"),
            UniversitySchool(schoolID: "LEED", name: "Leeds Skills Elective"),
            UniversitySchool(schoolID: "LLLC", name: "Lifelong Learning Centre"),
            UniversitySchool(schoolID: "MATH", name: "Mathematics"),
            UniversitySchool(schoolID: "MECH", name: "Mechanical Engineering"),
            UniversitySchool(schoolID: "COMM", name: "Media & Communication"),
            UniversitySchool(schoolID: "MEDS", name: "Medicine"),
            UniversitySchool(schoolID: "MEDV", name: "Medieval Studies"),
            UniversitySchool(schoolID: "MICR", name: "Microbiology"),
            UniversitySchool(schoolID: "CMNS", name: "Molecular Nanoscience"),
            UniversitySchool(schoolID: "MUSC", name: "Music"),
            UniversitySchool(schoolID: "PACI", name: "Performance and Cultural Sciences"),
            UniversitySchool(schoolID: "PRHS", name: "Philosophy, Religion and History of Science"),
            UniversitySchool(schoolID: "PHAS", name: "Physics and Astronomy"),
            UniversitySchool(schoolID: "PIED", name: "Politics and International Studies"),
            UniversitySchool(schoolID: "GPPH", name: "Primary/Palliative Care & Public Health"),
            UniversitySchool(schoolID: "PSIA", name: "Psychological and Social Medicine"),
            UniversitySchool(schoolID: "PSYC", name: "Psychology"),
            UniversitySchool(schoolID: "SLSP", name: "Sociology and Social Policy"),
            UniversitySchool(schoolID: "SPSC", name: "Sport and Exercice Science"),
            UniversitySchool(schoolID: "SABD", name: "Study Abroad"),
            UniversitySchool(schoolID: "TRAN", name: "Transport Studies")
        ]
        
    }
    
}
