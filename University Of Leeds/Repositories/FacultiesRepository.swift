//
//  FacultiesRepository.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 10/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import Foundation
import UIKit

class FacultiesRepository{
    
    let httpEntity = HTTPEntity()
    
    func retrieveFaculties(completion: @escaping (_ faculties: [UniversityFaculty]) -> ()){
        httpEntity.doPost(url: "faculties") { (data:[String:Any]?) in
            var universityFaculties:[UniversityFaculty] = []
            
            if data != nil{
                let facultiesObj = data!["faculties"] as! [[String:Any]]
                
                for i in 0...facultiesObj.count-1{
                    universityFaculties.append(UniversityFaculty(universityFacultyObject: facultiesObj[i]))
                }
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.universityFaculties = universityFaculties
            }
            
            completion(universityFaculties)
        }
        
    }
    
    func getAllFaculties()->[UniversityFaculty]{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.universityFaculties ?? []
    }
    
}
