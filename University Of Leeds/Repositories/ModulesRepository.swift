//
//  ModulesRepository.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 10/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import Foundation
import UIKit

class ModulesRepository{
    
    let httpEntity = HTTPEntity()
    let errorModule = UniversityModule(moduleID: "-1",name: "-1",isDiscoveryModule: false, isAvailableForIncomingStudyAbroad: false,schoolID: "-1",isUndergraduate: false)
    
    func retrieveModules(completion: @escaping (_ news: [UniversityModule]) -> ()){
        httpEntity.doPost(url: "modules") { (data: [String:Any]?) in
            var universityModules:[UniversityModule] = []
            
            if data != nil{
                let modulesObj = data!["modules"] as! [[String:Any]]
                
                for i in 0...modulesObj.count-1{
                    universityModules.append(UniversityModule(moduleObject: modulesObj[i]))
                }
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.universityModules = universityModules
            }else{
                universityModules.append(self.errorModule)
            }
            
            completion(universityModules)
        }
    }
    
    func getAllModules()->[UniversityModule]{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.universityModules ?? []
    }
    
    func getModulesForSchool(school: UniversitySchool)->[UniversityModule]{
        let allModules = getAllModules()
        print(allModules.count)
        var schoolModules:[UniversityModule] = []
        
        for module in allModules{
            if module.schoolID == school.schoolID{
                schoolModules.append(module)
            }
        }
        
        return schoolModules
    }
    
    func isModuleSaved(module: UniversityModule)->Bool{
        let savedModules = UserDefaults.standard.array(forKey: "savedModules") as? [String] ?? []
        return savedModules.contains(module.moduleID)
    }
    
    func setModuleSaved(module: UniversityModule, shouldSave: Bool){
        var savedModules = UserDefaults.standard.array(forKey: "savedModules") as? [String] ?? []
        
        if shouldSave {
            if !savedModules.contains(module.moduleID){
                savedModules.append(module.moduleID)
                UserDefaults.standard.set(savedModules, forKey: "savedModules")
            }
        }else{
            if let index = savedModules.firstIndex(of: module.moduleID){
                savedModules.remove(at: index)
                UserDefaults.standard.set(savedModules, forKey: "savedModules")
            }
        }
        
        print(savedModules)
    }
    
    func getSavedModules()->[String]{
        var returnModules:[String] = []

        let savedModules = UserDefaults.standard.array(forKey: "savedModules") as? [String] ?? []
        for module in savedModules {
            let moduleObj = self.findModuleById(moduleID: module)
            if moduleObj != nil{
                returnModules.append(moduleObj!.moduleID)
            }
        }
                
        return returnModules
    }
    
    func findModuleById(moduleID: String)->UniversityModule?{
        let allModules = getAllModules()
        
        for module in allModules {
            if module.moduleID == moduleID {
                return module
            }
        }
        
        return nil
    }
    
    func filterModules(modules:[UniversityModule], filter:String)->[UniversityModule]{
        var filtered:[UniversityModule] = []
        
        for module in modules{
            if module.moduleID.lowercased().contains(filter.lowercased()) || module.name.lowercased().contains(filter.lowercased()){
                filtered.append(module)
            }
        }
        
        return filtered
    }
}
