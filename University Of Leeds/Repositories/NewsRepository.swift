//
//  NewsRepository.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 03/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import Foundation
import UIKit

class NewsRepository{
    
    let httpEntity = HTTPEntity()
    
    let errorNews = UniversityNew(title: "-1",category: "-1",datetime: "-1",img_url: "-1",link_url: "-1")
    
    public func retrieveNews(completion: @escaping (_ news: [UniversityNew], _ events:[UniversityNew]) -> ()){
        
        httpEntity.doPost(url: "news_events"){ (data: [String:Any]?) in
            var allNews: [UniversityNew] = []
            var allEvents: [UniversityNew] = []
            if data != nil{
                let newsObj = data!["news"] as! [[String:Any]]
                let eventsObj = data!["events"] as! [[String:Any]]
                
                for i in 0...newsObj.count-1 {
                    allNews.append(UniversityNew(newsObject: newsObj[i]))
                }
                
                for i in 0...eventsObj.count-1{
                    allEvents.append(UniversityNew(newsObject: eventsObj[i]))
                }
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.universityNews = allNews
                appDelegate.universityEvents = allEvents
            }else{
                allEvents.append(self.errorNews)
                allNews.append(self.errorNews)
            }
            
            completion(allNews, allNews)

        }
    }
    
}
