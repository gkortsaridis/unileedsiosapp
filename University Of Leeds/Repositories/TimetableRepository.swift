//
//  TimetableRepository.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 03/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import Foundation
import Alamofire

class TimetableRepository{
    
    let httpEntity = HTTPEntity()
    let moduleRepository = ModulesRepository()
    
    let emptyModule = UniversityModule(moduleID: "-1", name: "-1", isDiscoveryModule: true, isAvailableForIncomingStudyAbroad: true, schoolID: "-1", isUndergraduate: true)
    
    let errorTimetableEntry = UniversityTimetableEntry(module:UniversityModule(moduleID: "-1", name: "-1", isDiscoveryModule: true, isAvailableForIncomingStudyAbroad: true, schoolID: "-1", isUndergraduate: true), activity: "-1", parentActivity: "-1", location: "-1", weekday: "-1", startTime: "-1", finishTime: "-1", teachingWeeksUniWeeks: "-1", teachingWeeksSplusWeeks: "-1", staff: "-1", recordingStatus: "-1")
    
    func createTimetableList(forModules: [String], completion: @escaping (_ data: [UniversityTimetableEntry]?) -> ()){
        let parameters:Parameters = ["modules":forModules]
        
        httpEntity.doPost(url: "timetable", withParameters: parameters) { (data: [String:Any]?) in
            if data != nil{
                let timetableObj = data!["timetable"] as! [[String:Any]]
                var allTimetables:[UniversityTimetableEntry] = []
                            
                if timetableObj.count > 0 {
                    for i in 0...timetableObj.count-1{
                        allTimetables.append(UniversityTimetableEntry(timetableObject: timetableObj[i]))
                    }
                    
                    var mondayTimetables:[UniversityTimetableEntry] = []
                    var tuesdayTimetables:[UniversityTimetableEntry] = []
                    var wednesdayTimetables:[UniversityTimetableEntry] = []
                    var thursdayTimetables:[UniversityTimetableEntry] = []
                    var fridayTimetables:[UniversityTimetableEntry] = []
                    
                    for timetable in allTimetables{
                        if timetable.weekday == "Monday"{
                            mondayTimetables.append(timetable)
                        }else if timetable.weekday == "Tuesday"{
                            tuesdayTimetables.append(timetable)
                        }else if timetable.weekday == "Wednesday"{
                            wednesdayTimetables.append(timetable)
                        }else if timetable.weekday == "Thursday"{
                            thursdayTimetables.append(timetable)
                        }else if timetable.weekday == "Friday"{
                            fridayTimetables.append(timetable)
                        }
                    }
                    
                    let timetableMonday = UniversityTimetableEntry(module: self.emptyModule, activity: "", parentActivity: "", location: "", weekday: "Monday", startTime: "", finishTime: "", teachingWeeksUniWeeks: "", teachingWeeksSplusWeeks: "", staff: "", recordingStatus: "")
                    
                    let timetableTuesday = UniversityTimetableEntry(module: self.emptyModule, activity: "", parentActivity: "", location: "", weekday: "Tuesday", startTime: "", finishTime: "", teachingWeeksUniWeeks: "", teachingWeeksSplusWeeks: "", staff: "", recordingStatus: "")
                    
                    let timetableWednesday = UniversityTimetableEntry(module: self.emptyModule, activity: "", parentActivity: "", location: "", weekday: "Wednesday", startTime: "", finishTime: "", teachingWeeksUniWeeks: "", teachingWeeksSplusWeeks: "", staff: "", recordingStatus: "")
                    
                    let timetableThursday = UniversityTimetableEntry(module: self.emptyModule, activity: "", parentActivity: "", location: "", weekday: "Thursday", startTime: "", finishTime: "", teachingWeeksUniWeeks: "", teachingWeeksSplusWeeks: "", staff: "", recordingStatus: "")
                    
                    let timetableFriday = UniversityTimetableEntry(module: self.emptyModule, activity: "", parentActivity: "", location: "", weekday: "Friday", startTime: "", finishTime: "", teachingWeeksUniWeeks: "", teachingWeeksSplusWeeks: "", staff: "", recordingStatus: "")
                    
                    
                    var returnArray:[UniversityTimetableEntry] = [timetableMonday]
                    for t in mondayTimetables{ returnArray.append(t) }
                    returnArray.append(timetableTuesday)
                    for t in tuesdayTimetables{ returnArray.append(t) }
                    returnArray.append(timetableWednesday)
                    for t in wednesdayTimetables{ returnArray.append(t) }
                    returnArray.append(timetableThursday)
                    for t in thursdayTimetables{ returnArray.append(t) }
                    returnArray.append(timetableFriday)
                    for t in fridayTimetables{ returnArray.append(t) }
                    completion(returnArray)
                }else{
                    completion([])
                }
            }else{
                completion([self.errorTimetableEntry])
            }
            
        }
        
        
    }
}
