//
//  MySettingsViewController.swift
//  University Of Leeds
//
//  Created by George Kortsaridis on 20/07/2019.
//  Copyright © 2019 GeorgeKortsaridis. All rights reserved.
//

import UIKit

class MySettingsViewController: UIViewController {

    @IBOutlet weak var startupSegment: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let startupScreen = UserDefaults.standard.string(forKey: "startupScreen") ?? ""
        if startupScreen == "mySchedule" {
            startupSegment.selectedSegmentIndex = 1
        }else{
            startupSegment.selectedSegmentIndex = 0
        }

        startupSegment.addTarget(self, action: #selector(MySettingsViewController.indexChanged(_:)), for: .valueChanged)

    }
    
    @IBAction func feedback(_ sender: Any) {
        guard let url = URL(string: "https://docs.google.com/forms/d/e/1FAIpQLSevxRkPXn6aD0Q4UTxrTnOJCZasabFyhe0u4IVvPGKG_cQxiA/viewform?usp=sf_link")
        else {
            return
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @objc func indexChanged(_ sender: UISegmentedControl) {
        if startupSegment.selectedSegmentIndex == 0 {
            UserDefaults.standard.set("freshWall", forKey: "startupScreen")
        } else {
            UserDefaults.standard.set("mySchedule", forKey: "startupScreen")
        }
    }
}
